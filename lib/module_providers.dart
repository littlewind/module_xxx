import 'package:flutter_app_module_xxx/login_provider.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class ModuleProvider {
  factory ModuleProvider() {
    if (_instance == null) {
      _instance = ModuleProvider._getInstance();
    }
    return _instance;
  }

  static ModuleProvider _instance;
  ModuleProvider._getInstance();

  // Declare list of model here
  LoginModel login;

  init() {
    login = LoginModel();
  }

  List<SingleChildWidget> get provides => [
    ChangeNotifierProvider(create: (_) => login),
  ];

}